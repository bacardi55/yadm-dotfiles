# bacardi55 (bac@rdi55.pl)
# Last modified 2019/05/21

##########################################################################
#  IMAP - Neomutt configuration
##########################################################################
# -------------------------------------------------------------------------
# Name:
# -------------------------------------------------------------------------
# This variable specifies what “real” or “personal” name should be used
# when sending mes‐ sages.  If not specified, then the user's “real name”
# will  be  read  from  /etc/passwd.   This option will not be used, if
# “$from” is set.
# -------------------------------------------------------------------------
set realname = "{first,last}name"

# -------------------------------------------------------------------------
# Name: from
# -------------------------------------------------------------------------
# When set, this variable contains a default “from” address.  It can be
# overridden  using “my_hdr” (including from a “send-hook”) and
# $reverse_name.  This variable is ignored if $use_from is unset.  If not
# specified, then it may be read from the environment variable $EMAIL.
# -------------------------------------------------------------------------
set from="me@mail.com"

# -------------------------------------------------------------------------
# Name: spoolfile
# -------------------------------------------------------------------------
# If your spool mailbox is in a non-default place where NeoMutt cannot find
# it,  you  can specify its location with this variable.  If not specified,
# then the environment variables $MAIL and $MAILDIR will be checked.
# -------------------------------------------------------------------------
set folder = "imaps://me@mail.com@server:port/INBOX/"
set spoolfile = "imaps://me@mail.com@server:port/INBOX/"

# -------------------------------------------------------------------------
# Name: mailboxes
# -------------------------------------------------------------------------
# The mailboxes  specifies  folders which can receive mail and which will
# be checked for new messages. When changing folders, pressing space will
# cycle through folders with new mail.  The named-mailboxes is an
# alternative to mailboxes that allows adding a description for a mailbox.
# NeoMutt can be configured to display the description instead of the
# mailbox path.  The unmailboxes  command  is used to remove a file name
# from the list of folders which can receive mail.  If “*” is specified as
# the file name, the list is emptied.
# -------------------------------------------------------------------------
# unmailboxes *
# mailboxes +Drafts +Sent +Spam
mailboxes $spoolfile
mailboxes "imaps://me@mail.com@server:port/Sent"
mailboxes "imaps://me@mail.com@server:port/Drafts"

# -------------------------------------------------------------------------
# Name: imap_user
# -------------------------------------------------------------------------
# The name of the user whose mail you intend to access on the IMAP server.
# This variable defaults to your user name on the local machine.
set imap_user = "me@mail.com"

# -------------------------------------------------------------------------
# Name: imap_pass
# -------------------------------------------------------------------------
# Specifies the password for your IMAP account.  If unset, NeoMutt will
# prompt you for your password when you invoke the <imap-fetch-mail>
# function or try to open an IMAP folder.
#
# Warning: you should only use this option when you are on a fairly secure
# machine, because the superuser can read your neomuttrc even if you are
# the only one who can read the file.
# set imap_pass = "imappasswd"

# Les mailboxes sont les autres dossiers ou les mails seront vérfiés :
#mailboxes = "iSent +Spam
# mailboxes $spoolfile


# L'adresse du serveur smtp
set smtp_url = "smtps://me@mail.com@server:port/"
set smtp_authenticators = "login"
# set ssl_starttls = no
# set ssl_force_tls = no

# Pour copier les mails envoyés sur le serveur (mailboxe sent)
set record = "imaps:///Sent"

# Les adresses alternatives auxquelles on peut recevoir un email
alternates '^me2@gmail\.com$|^me3@gmail\.com$'

# Le tri (ici ordre inversé : les derniers mails en haut)
set sort = "reverse-threads"

set timeout = 15
set mail_check = 60

set charset = "utf-8"
#set locale = "fr_FR.UTF-8"

set editor = "vim" # what else...

#set sort = threads
set sort = "reverse-threads"
set sort_aux=reverse-date-received	# puis par date

alternative_order text/plain text/enriched text/html
# auto_view text/html

# option before quitting
set quit = "ask-no"

# beep new mail.
set beep_new = yes

set signature =~/.muttrc_signature

set sidebar_visible = yes
set sidebar_short_path = yes
# --------------------------------------------------------------------------
# FUNCTIONS – shown with an example mapping
# --------------------------------------------------------------------------
# Move the highlight to the previous mailbox
bind index,pager \Cp sidebar-prev
# Move the highlight to the next mailbox
bind index,pager \Cn sidebar-next
# Open the highlighted mailbox
bind index,pager \Co sidebar-open
# Move the highlight to the previous page
# This is useful if you have a LOT of mailboxes.
bind index,pager <F3> sidebar-page-up
# Move the highlight to the next page
# This is useful if you have a LOT of mailboxes.
bind index,pager <F4> sidebar-page-down
# Move the highlight to the previous mailbox containing new, or flagged,
# mail.
bind index,pager <F5> sidebar-prev-new
# Move the highlight to the next mailbox containing new, or flagged, mail.
bind index,pager <F6> sidebar-next-new
# Toggle the visibility of the Sidebar.
bind index,pager B sidebar-toggle-visible
