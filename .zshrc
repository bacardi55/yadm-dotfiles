# bacardi55 (bac@rdi55.pl)
# Last modified 2019/05/21

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="gnzh"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git composer catimg command-not-found common-aliases compleat dirhistory docker symfony2 taskwarrior web-search colorize cp extract history vundle tmux bgnotify)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="/home/bacardi55/bin/:/home/bacardi55/.local/bin/:/home/bacardi55/.cargo/bin:$PATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

bgnotify_threshold=10

export EDITOR=vim
export VISUAL=gvim
export BROWSER="firefox-trunk"
export PAGER="most -s"
#export XTERM="urxvtc"

# Aliases
alias top="htop"
alias more="most -s"
#alias less="most -s"
alias df='dfc'
alias du='du -hc'
#alias ls="exa"

# backlight alias
alias lightp="xbacklight -inc 20"
alias lightm="xbacklight -dec 20"
alias lightmax="xbacklight -set 100"
alias lightmin="xbacklight -set 15"

# tmux
alias tmux="TERM=xterm-256color tmux"

# X app.
alias nautilus="nautilus --no-desktop"
alias chrome="chromium-browser"
alias google-chrome="chromium-browser"

# i3
alias i3MoveToOutput="i3-msg move workspace to output"

# proxy socks
#alias proxy_socks='ssh -D 5569 bacardi55@hyunkel -p22 -N'

# Gvim
[[ -s "/home/bacardi55/.gvm/scripts/gvm" ]] && source "/home/bacardi55/.gvm/scripts/gvm"

# big files
alias biggy="find . -type f -printf '%s %p\n'| sort -nr | head -20"

# todo.sh / Topydo
alias todo="todo.sh"
alias topidoc="topydo columns"
alias topydow="topydo -c ~/.config/topydo/config_acquia"
alias topydowc="topydo -c ~/.config/topydo/config_acquia columns -l ~/.config/topydo/columns_acquia"
alias tc="topydo columns"
alias tw="topydo -c ~/.config/topydo/config_acquia"
alias twc="topydo -c ~/.config/topydo/config_acquia columns -l ~/.config/topydo/columns_acquia"

# Docker
alias dcp="docker-compose"

# Lazydocker
alias lzd='docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v /home/bacardi55/.config/lazydocker:/.config/jesseduffield/lazydocker lazyteam/lazydocker'

#alias nvim="~/.vim/bundle/nvim/nvim nvim"

# Jrnl:
alias jrnlw="jrnl work"
alias jrnlp="jrnl perso"

mkcd () {
  case "$1" in
    */..|*/../) cd -- "$1";; # that doesn't make any sense unless the directory already exists
    /*/../*) (cd "${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd -- "$1";;
    /*) mkdir -p "$1" && cd "$1";;
    */../*) (cd "./${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd "./$1";;
    ../*) (cd .. && mkdir -p "${1#.}") && cd "$1";;
    *) mkdir -p "./$1" && cd "./$1";;
  esac
}

# createNotes() {
#   NOW=$(date +"%F")
#   FILE="$NOW.notes.$1"
#   vim "$FILE"
# }

bindkey \^U backward-kill-line

#eval $(thefuck --alias)
